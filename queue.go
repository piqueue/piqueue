package main

import "sync"

// Queue is a list of jobs to be executed
type Queue struct {
	Name     string
	Priority int
	NextJob  *Job
	LastJob  *Job
	Mutex    sync.RWMutex
}

// Enqueue a job to this queue
func (q *Queue) Enqueue(j *Job) {
	q.Mutex.Lock()
	defer q.Mutex.Unlock()

	if q.NextJob == nil {
		q.NextJob = j
		q.LastJob = j
	} else if q.LastJob != nil {
		q.LastJob.Next = j
		q.LastJob = j
	}
}

// Dequeue a job from this queue
func (q *Queue) Dequeue() *Job {
	q.Mutex.Lock()
	defer q.Mutex.Unlock()

	if q.NextJob == nil {
		return nil
	}

	var job = q.NextJob
	q.NextJob = q.NextJob.Next
	return job
}

// Count the number of jobs in this queue
func (q *Queue) Count() int {
	q.Mutex.RLock()
	defer q.Mutex.RUnlock()

	var count int
	for job := q.NextJob; job != nil; job = job.Next {
		count++
	}
	return count
}
