package main

import "github.com/BurntSushi/toml"

// Config is the main configuration structure usually loaded from disk
type Config struct {
	Pools map[string]poolConfig
}

type poolConfig struct {
	Name   string
	Key    string
	Queues map[string]queueConfig
}

type queueConfig struct {
	Priority int
}

// LoadConfigFromFile finds the configuration for piqueue and loads it
func LoadConfigFromFile() (*Config, error) {
	var config Config
	if _, err := toml.DecodeFile("config.toml", &config); err != nil {
		return nil, err
	}
	return &config, nil
}
