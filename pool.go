package main

import (
	"errors"
	"sync"
	"time"
)

// Pool is a collection of queues
type Pool struct {
	Key          string
	QueuesByName map[string]*Queue
	QueueSlots   []*poolQueueSlot
	WorkersByID  map[string]*Worker
	Mutex        sync.RWMutex
}

type poolQueueSlot struct {
	Priority int
	Mutex    sync.Mutex
	First    *poolQueueSlotEntry
}

type poolQueueSlotEntry struct {
	Queue *Queue
	Next  *poolQueueSlotEntry
	Prev  *poolQueueSlotEntry
}

func newPoolQueueSlotEntry(q *Queue) *poolQueueSlotEntry {
	entry := &poolQueueSlotEntry{Queue: q}
	entry.Next = entry
	entry.Prev = entry
	return entry
}

// AddQueue adds a queue to the pool.
// The higher the priority value, the more priority the queue takes
func (p *Pool) AddQueue(q *Queue) error {
	if q.Name == "" {
		return errors.New("Queue must have a name.")
	}

	p.Mutex.Lock()
	defer p.Mutex.Unlock()

	if p.QueuesByName[q.Name] != nil {
		return errors.New("Queue with that name already exists.")
	}

	// Perform an insertion sort based on priority.
	// slice element 0 is lower priority than slice element 1
	var inserted = false
	for i, slot := range p.QueueSlots {
		if slot.Priority == q.Priority {
			// Matched priority, insert into this slot.
			slot.Mutex.Lock()
			entry := &poolQueueSlotEntry{
				Queue: q,
				Next:  slot.First,
				Prev:  slot.First.Prev,
			}
			slot.First.Prev.Next = entry
			slot.First.Prev = entry
			slot.First = entry

			slot.Mutex.Unlock()
			inserted = true
			break
		} else if slot.Priority < q.Priority {
			// Insert at this position
			slot := &poolQueueSlot{
				Priority: q.Priority,
				First:    newPoolQueueSlotEntry(q),
			}
			p.QueueSlots = append(p.QueueSlots[:i], append([]*poolQueueSlot{slot}, p.QueueSlots[i:]...)...)
			inserted = true
			break
		}
	}
	if !inserted {
		p.QueueSlots = append(p.QueueSlots, &poolQueueSlot{
			Priority: q.Priority,
			First:    newPoolQueueSlotEntry(q),
		})
	}
	if p.QueuesByName == nil {
		p.QueuesByName = make(map[string]*Queue)
	}
	p.QueuesByName[q.Name] = q
	return nil
}

// Enqueue a job within the pool
func (p *Pool) Enqueue(j *Job) error {
	if j.QueueName == "" {
		return errors.New("Job can't be queued without a queue name")
	}

	p.Mutex.RLock()
	defer p.Mutex.RUnlock()

	q := p.QueuesByName[j.QueueName]
	if q == nil {
		return errors.New("No queue found matching job.")
	}

	q.Enqueue(j)
	return nil
}

// Dequeue the next job in priority from this pool
func (p *Pool) Dequeue() *Job {
	p.Mutex.RLock()
	defer p.Mutex.RUnlock()

	for _, slot := range p.QueueSlots {
		job := dequeueFromSlot(slot)
		if job != nil {
			return job
		}
	}
	return nil
}

func dequeueFromSlot(slot *poolQueueSlot) *Job {
	slot.Mutex.Lock()
	defer slot.Mutex.Unlock()

	// Strategy: Dequeue in order from head to tail
	// Move the first queue that has an entry to tail
	var prev *poolQueueSlotEntry
	current := slot.First
	first := current
	for prev == nil || current != first {
		job := current.Queue.Dequeue()
		if job != nil {
			slot.First = current.Next
			return job
		}
		prev = current
		current = current.Next
	}
	return nil
}

// AddOrUpdateWorker checks a worker in
func (p *Pool) AddOrUpdateWorker(worker *Worker) {
	p.Mutex.Lock()
	defer p.Mutex.Unlock()

	stored := p.WorkersByID[worker.ID]
	if stored == nil {
		if p.WorkersByID == nil {
			p.WorkersByID = make(map[string]*Worker)
		}
		p.WorkersByID[worker.ID] = worker
		stored = worker
	}
	stored.LastSeenAt = time.Now()
}

// Worker returns the worker identified by id
func (p *Pool) Worker(id string) *Worker {
	p.Mutex.RLock()
	defer p.Mutex.RUnlock()

	return p.WorkersByID[id]
}
