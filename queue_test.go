package main

import (
	"strconv"
	"sync"
	"testing"
)

func TestQueueSimple(t *testing.T) {
	var q Queue
	if q.Count() != 0 {
		t.Error("Empty queue should be empty")
	}
	var originalJob = &Job{
		Name: "Test Job",
	}
	q.Enqueue(originalJob)
	if q.Count() != 1 {
		t.Error("Queue should contain one job after enqueue")
	}
	var j = q.Dequeue()
	if q.Count() != 0 {
		t.Error("Queue should be empty after dequeue")
	}
	if j != originalJob {
		t.Error("Job was not the same job we enqueued")
	}
}

func queuer(q *Queue, start int, end int, c chan bool) {
	for i := start; i < end; i++ {
		q.Enqueue(&Job{Name: strconv.Itoa(i)})
	}
	c <- true
}

func processor(q *Queue, mutex *sync.Mutex, results map[string]bool, doneProcessing chan bool, fullyQueued *bool) {
	for {
		var j = q.Dequeue()

		if j == nil {
			mutex.Lock()
			if *fullyQueued {
				mutex.Unlock()
				break
			}
			mutex.Unlock()
		} else {
			mutex.Lock()
			results[j.Name] = true
			mutex.Unlock()
		}
	}
	doneProcessing <- true
}

func TestQueueConcurrency(t *testing.T) {
	var q Queue
	var results = make(map[string]bool)
	var fullyQueued bool
	var mutex sync.Mutex

	// Make some concurrent dequeuers
	doneProcessing := make(chan bool, 10)
	for i := 0; i < 10; i++ {
		go processor(&q, &mutex, results, doneProcessing, &fullyQueued)
	}

	// Start the queuers
	var queueChan = make(chan bool)
	go queuer(&q, 0, 10000, queueChan)
	go queuer(&q, 10000, 20000, queueChan)
	go queuer(&q, 20000, 30000, queueChan)
	if <-queueChan && <-queueChan && <-queueChan {
		mutex.Lock()
		fullyQueued = true
		mutex.Unlock()
	}

	// Wait for the queue to be empty
	// This is intentionally bad performance to stress reading and writing concurrency
	for q.Count() > 0 {
	}

	for i := 0; i < 10; i++ {
		<-doneProcessing
	}

	for i := 0; i < 30000; i++ {
		if !results[strconv.Itoa(i)] {
			t.Error("Results were not correct.")
		}
	}
}
