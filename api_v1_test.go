package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
)

func initTestServer() {
	sServer = &Server{}
	sServer.Pools = make(map[string]*Pool)
	sServer.Pools["pool1"] = &Pool{Key: "secretkey"}
	_ = sServer.Pools["pool1"].AddQueue(&Queue{Name: "queue1"})
	_ = sServer.Pools["pool1"].AddQueue(&Queue{Name: "queue2"})

}

type apiResponse struct {
	code    int
	content map[string]interface{}
}

func getAPI(url string, t *testing.T) *apiResponse {
	res, err := http.Get(url)
	if err != nil {
		t.Errorf("Error retrieving %v: %v", url, err)
		return nil
	}

	return decodeResponse(res, t)
}

func postAPI(url string, data url.Values, t *testing.T) *apiResponse {
	res, err := http.PostForm(url, data)
	if err != nil {
		t.Errorf("Error posting %v: %v", url, err)
		return nil
	}

	return decodeResponse(res, t)
}

func decodeResponse(res *http.Response, t *testing.T) *apiResponse {
	ar := &apiResponse{code: res.StatusCode}
	data, _ := ioutil.ReadAll(res.Body)
	err := json.Unmarshal(data, &ar.content)
	if err != nil {
		t.Errorf("Error parsing JSON response: %v", err)
	}
	return ar
}

func TestAPI(t *testing.T) {
	server := httptest.NewServer(httpHandler())
	defer server.Close()
	initTestServer()

	r := getAPI(server.URL+"/api/v1/pools", t)
	pools := r.content["pools"].([]interface{})
	if len(pools) != 1 {
		t.Error("Expected 1 pool from API")
	}
	if pools[0].(string) != "pool1" {
		t.Error("Expected pool1")
	}

	r = getAPI(server.URL+"/api/v1/pool/pool1?key=secretkey", t)
	queues := r.content["queues"].(map[string]interface{})
	if len(queues) != 2 {
		t.Error("Expected 2 queues from API")
	}
	if queues["queue1"] == nil {
		t.Error("Expected queue1 in resultant queues")
	}

	// /pool/poolname -- put
	r = postAPI(server.URL+"/api/v1/pool/pool1?key=secretkey", url.Values{
		"name":  {"job1"},
		"queue": {"queue1"},
	}, t)
	if len(r.content) > 0 {
		t.Error("Expected empty response from API")
	}

	// /pool/poolname/worker/:Workerid" -- post
	r = postAPI(server.URL+"/api/v1/pool/pool1/worker/worker1?key=secretkey", url.Values{"id": {"localhost:1234"}}, t)
	if len(r.content) > 0 {
		t.Error("Expected empty response from API")
	}
	// /pool/poolname/worker/worker/job" -- get
	// "" post

}
