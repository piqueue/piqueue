package main

import "time"

// Job represents a function of work to be done
type Job struct {
	Name        string `form:"name" binding:"required"`
	QueueName   string `form:"queue" binding:"required"`
	CreatedAt   time.Time
	StartedAt   time.Time
	CompletedAt time.Time
	ErroredAt   time.Time
	Error       error
	Payload     interface{}

	Next *Job
}
