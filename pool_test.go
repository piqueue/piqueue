package main

import (
	"strings"
	"testing"
)

type poolTestData struct {
	queues     []poolTestQueue
	jobs       []string
	checkNames []string
}

type poolTestQueue struct {
	name     string
	priority int
}

func checkedEnqueue(t *testing.T, p *Pool, name string, queue string) {
	if err := p.Enqueue(&Job{Name: name, QueueName: queue}); err != nil {
		t.Error(err)
	}
}

func checkDequeue(t *testing.T, p *Pool, name string) {
	j := p.Dequeue()
	if j == nil {
		t.Errorf("Expected Job %v, got nil", name)
	} else if j.Name != name {
		t.Errorf("Expected Job %v, got %v", name, j.Name)
	}
}

func TestPool(t *testing.T) {
	matrix := []poolTestData{
		poolTestData{
			[]poolTestQueue{
				poolTestQueue{"a", 0},
			},
			[]string{
				"a.1",
				"a.2",
			},
			[]string{
				"a.1",
				"a.2",
			},
		},
		poolTestData{
			[]poolTestQueue{
				poolTestQueue{"a", 0},
				poolTestQueue{"b", 0},
			},
			[]string{
				"a.1",
				"a.2",
				"b.1",
				"b.2",
			},
			[]string{
				"b.1",
				"a.1",
				"b.2",
				"a.2",
			},
		},
		poolTestData{
			[]poolTestQueue{
				poolTestQueue{"a", 0},
				poolTestQueue{"b", 1},
			},
			[]string{
				"a.1",
				"a.2",
				"b.1",
				"b.2",
			},
			[]string{
				"b.1",
				"b.2",
				"a.1",
				"a.2",
			},
		},
		poolTestData{
			[]poolTestQueue{
				poolTestQueue{"a", 0},
				poolTestQueue{"b", 0},
				poolTestQueue{"c", 0},
			},
			[]string{
				"a.1",
				"a.2",
				"b.1",
				"b.2",
				"c.1",
				"c.2",
			},
			[]string{
				"c.1",
				"b.1",
				"a.1",
				"c.2",
				"b.2",
				"a.2",
			},
		},
	}

	for _, data := range matrix {
		p := &Pool{}
		for _, queue := range data.queues {
			if err := p.AddQueue(&Queue{Name: queue.name, Priority: queue.priority}); err != nil {
				t.Error(err)
			}
		}
		for _, job := range data.jobs {
			parts := strings.Split(job, ".")
			checkedEnqueue(t, p, job, parts[0])
		}
		for _, check := range data.checkNames {
			checkDequeue(t, p, check)
		}
	}
}
