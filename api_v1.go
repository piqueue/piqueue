package main

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

func apiV1PoolsList(c *gin.Context) {
	server := GetServer()
	pools := make([]string, 0, len(server.Pools))
	for poolName := range server.Pools {
		pools = append(pools, poolName)
	}
	c.JSON(http.StatusOK, gin.H{"pools": pools})
}

func poolKeyInvalid(pool *Pool, c *gin.Context) bool {
	if pool.Key != c.Query("key") {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid key for this pool"})
		return true
	}
	return false
}

func apiV1PoolListQueues(c *gin.Context) {
	server := GetServer()

	pool := server.Pools[c.Param("poolName")]
	if pool == nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Pool not found"})
		return
	}

	if poolKeyInvalid(pool, c) {
		return
	}

	pool.Mutex.RLock()
	queues := make(map[string]interface{})
	for _, queue := range pool.QueuesByName {
		queues[queue.Name] = gin.H{
			"priority": queue.Priority,
		}
	}
	pool.Mutex.RUnlock()
	c.JSON(http.StatusOK, gin.H{"queues": queues})
}

func apiV1PoolUpdateWorker(c *gin.Context) {
	server := GetServer()

	pool := server.Pools[c.Param("poolName")]
	if pool == nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Pool not found"})
		return
	}

	if poolKeyInvalid(pool, c) {
		return
	}

	worker := pool.Worker(c.Param("worker"))
	if worker == nil {
		worker = &Worker{
			ID: c.Param("worker"),
		}
	}
	pool.AddOrUpdateWorker(worker)
	c.JSON(http.StatusOK, gin.H{})
}

func apiV1PoolDequeueJob(c *gin.Context) {
	server := GetServer()

	pool := server.Pools[c.Param("poolName")]
	if pool == nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Pool not found"})
		return
	}

	if poolKeyInvalid(pool, c) {
		return
	}

	worker := pool.Worker(c.Param("worker"))
	if worker == nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Worker not found"})
		return
	}

	job := pool.Dequeue()
	if job == nil {
		c.JSON(http.StatusOK, gin.H{})
		return
	}

	worker.CurrentJob = job

	c.JSON(http.StatusOK, gin.H{
		"name":       job.Name,
		"queue":      job.QueueName,
		"created_at": job.CreatedAt.Format(time.RFC3339),
		"payload":    job.Payload,
	})
}

func apiV1PoolEnqueueJob(c *gin.Context) {
	server := GetServer()

	pool := server.Pools[c.Param("poolName")]
	if pool == nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Pool not found"})
		return
	}

	if poolKeyInvalid(pool, c) {
		return
	}

	var job Job
	if err := c.Bind(&job); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	job.CreatedAt = time.Now()
	if len(c.PostForm("payload")) > 0 {
		if err := json.Unmarshal([]byte(c.PostForm("payload")), &job.Payload); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Payload is invalid JSON."})
			return
		}
	}

	if err := pool.Enqueue(&job); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}
	c.JSON(http.StatusOK, gin.H{})
}

func apiV1PoolCompleteJob(c *gin.Context) {
	server := GetServer()

	pool := server.Pools[c.Param("poolName")]
	if pool == nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Pool not found"})
		return
	}

	if poolKeyInvalid(pool, c) {
		return
	}

	worker := pool.Worker(c.Param("worker"))
	if worker == nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Worker not found"})
		return
	}

	worker.CurrentJob = nil
	c.JSON(http.StatusOK, gin.H{})
}
