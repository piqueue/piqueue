package main

import log "github.com/Sirupsen/logrus"

func main() {
	config, err := LoadConfigFromFile()
	if err != nil {
		log.Fatal("Error loading configuration", err)
	}
	log.Info("Starting server.")

	RunServer(config)
}
