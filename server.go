package main

import (
	"net/http"

	log "github.com/Sirupsen/logrus"
	"github.com/fvbock/endless"
	"github.com/gin-gonic/gin"
)

// Server is a singleton wrapping the logic for the entirety of the server
type Server struct {
	Pools map[string]*Pool
}

var sServer *Server

// GetServer returns the singleton instance of server
func GetServer() *Server {
	return sServer
}

// RunServer launches the web server
func RunServer(config *Config) {
	sServer = &Server{}
	sServer.Pools = make(map[string]*Pool)
	log.Infof("Running server...")
	for poolName, poolConfig := range config.Pools {
		log.Infof("Loading %v", poolName)
		if err := sServer.createPoolFromConfig(poolName, poolConfig); err != nil {
			log.Fatal("Failed to create pool", poolName, err)
		}
	}

	sServer.startServer()
}

func (server *Server) createPoolFromConfig(name string, config poolConfig) error {
	pool := Pool{
		Key: config.Key,
	}

	for queueName, queueConfig := range config.Queues {
		if err := pool.AddQueue(&Queue{Name: queueName, Priority: queueConfig.Priority}); err != nil {
			return err
		}
	}

	server.Pools[name] = &pool

	return nil
}

func (server *Server) startServer() {
	err := endless.ListenAndServe("localhost:4567", httpHandler())
	if err != nil {
		log.Fatal("Failure to start server", err)
	}
}

func httpHandler() *gin.Engine {
	router := gin.Default()

	router.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"status": "Blah!"})
	})

	api := router.Group("/api")
	{
		v1 := api.Group("/v1")
		{
			v1.GET("/pools", apiV1PoolsList)
			v1.GET("/pool/:poolName", apiV1PoolListQueues)
			v1.POST("/pool/:poolName", apiV1PoolEnqueueJob)
			v1.POST("/pool/:poolName/worker/:worker", apiV1PoolUpdateWorker)
			v1.GET("/pool/:poolName/worker/:worker/job", apiV1PoolDequeueJob)
			v1.POST("/pool/:poolName/worker/:worker/job", apiV1PoolCompleteJob)
		}
	}

	return router
}
