# Piqueue [![build status](https://gitlab.com/piqueue/piqueue/badges/master/build.svg)](https://gitlab.com/piqueue/piqueue/commits/master) [![codecov](https://codecov.io/gl/piqueue/piqueue/branch/master/graph/badge.svg?token=tMEiMVU1K3)](https://codecov.io/gl/piqueue/piqueue)


__This document is a work in progress and is currently a freeform collection of thoughts. It does not represent what is currently available in this project.__

Piqueue (pronounced puh-queue) is a centralized queuing service that has several goals:

* An easy to use, easy to deploy queuing service with a simple HTTP RESTful API
  * API should include simple GET operations as well as long-polling support for notifying workers
  * API should not only support JSON but also a binary format (MessagePack?)
* Highly-scalable
* "Next job" queries restrictable by logic, such as "never run more than 5 aggregation jobs at once"
* Centralized queue logic management, including allowing/preventing duplicate jobs, automatic retry logic, error reporting logic
* Scheduled job queueing
* Multi-application support
* Provide historical aggregate information

## Basic Concepts

### Pools

A pool in Piqueue is at its core is a named entity that has a grouping of queues, workers, rules, and schedules. Each pool also has its own secret key to ensure separate projects cannot interfere with each other's queues. Pools are a construct designed to allow for easy grouping of workers. For example, you may have two projects that have their own workers and sets of jobs. In this setup, you would use two pools. If you have three projects, one which is the backend logic and does all the processing and the other two just enqueue jobs, you would use one pool.

### Workers

A worker is a process that interacts with the API to retrieve next jobs and report success/failure of the job. Workers belong to a single pool, and can ask for jobs from any queue in that pool or individual queues.

### Queues

A queue is a named entity that represents a [FIFO](https://en.wikipedia.org/wiki/FIFO_(computing_and_electronics)) list of jobs. The only time that jobs are not processed in a FIFO manner are when rules are set up that enforce limits that would be broken if the next job in queue were to be processed next. Queues are ordered within the pool by priority tiers, allowing some queues to always execute before other queues.

### Rules

Rules are a mechanism designed to put constraints on what workers can be doing at the same time. For example, imagine if you have an aggregation background process that is very read and write intensive on the database server. You've noticed that if you have 2 running at once, the database is still able to keep up with regular queries in addition to the aggregation process. However, if you have 3 running at once, the server becomes overloaded and performance is impacted. A rule could be added to ensure that only up to 2 aggregation jobs are able to execute at one time.

### Schedules 

Cron is annoying to deal with sometimes. While you could write a cron job that queues up jobs, you have to ensure you have monitoring set up for it in case it fails. Schedules in Piqueue are meant to simplify this greatly, offering the ability to automatically enqueue a job into a queue with a payload based on a schedule set on the server.

### Jobs

A Job is a named entity describing an operation that needs to take place. It belongs to a Queue in a Pool, and has an optional payload of JSON-compatible data.

## Development Milestones

1. - insert creative code name here -
  * API is available
  * Functionality available: Pools, Workers, Queues, Schedules
  * Most configuration is done via config files
  * Queues are not persisted
2. - insert creative code name here -
  * PostgreSQL backend written
    * trigger-based partitioning of job history to allow for easy cleanup
    * All operations properly done with atomicity in mind to ensure multiple Piqueue servers could use the same database backend for HA setups.
  * Queues are persisted
  * Most configruation is still done via config files
3. - insert creative code name here -
  * Rules support and initial design on how rules can optimally be processed at the database layer if possible -- PSQL Javascript stored proc? Hairy pqpsql procs that are dynamically updated?
4. - insert creative code name here -
  * Internal worker pool and schedule for job history cleanup as well as historical aggregation
    * Should there be user-definable aggregation metrics beyond count, average time, stddev per pool/queue/job?

Stuff that needs to be slotted, but unsure of where:

* Notification support
* Dashboard
* Web based configuration
