package main

import "time"

// Worker is a structure representing a process on a server that processes jobs
type Worker struct {
	ID         string
	LastSeenAt time.Time
	CurrentJob *Job
}
